import Vue from 'vue';
import './plugins/axios';
import App from './App.vue';
import router from './router';
import store from './store';
import VueMaterial from 'vue-material';
import 'vue-material/dist/vue-material.min.css';
import Toasted from 'vue-toasted';
import axios from 'axios';

const ToastedOptions = {
  position: 'bottom-center',
  duration: 1500,
  theme: 'bubble'
};

Vue.use(Toasted, ToastedOptions);
Vue.use(VueMaterial);

Vue.config.productionTip = false;

router.beforeEach((to, from, next) => {
  if (!localStorage.getItem('token') && to.name !== 'Login') next({ name: 'Login' });
  next();
});

new Vue({
  router,
  store,
  render: (h) => h(App)
}).$mount('#app');
