import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Login',
    component: () => import('@/components/LogIn.vue'),
    beforeEnter: (to, from, next) => {
      if (localStorage.getItem('token')) next('/products');
      next();
    }
  },
  {
    path: '/products',
    name: 'Products',
    component: () => import('@/components/Layouts/Products.vue')
  },
  {
    path: '/bidsasks',
    name: 'BidsAsks',
    component: () => import('@/components/Layouts/BidsAsks.vue')
  },
  {
    path: '/buysell',
    name: 'BuySell',
    component: () => import('@/components/Layouts/BuySell.vue')
  },
  {
    path: '/orders',
    name: 'Orders',
    component: () => import('@/components/Layouts/Orders.vue')
  },
  {
    path: '/settings',
    name: 'Settings',
    component: () => import('@/components/Layouts/Settings.vue')
  },
  {
    path: '/addproduct/:id',
    name: 'AddProduct',
    component: () => import('@/components/Layouts/AddProduct.vue')
  },
  {
    path: '*',
    redirect: '/products'
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

export default router;
