import imageCompression from 'browser-image-compression';
import { mapState } from 'vuex';

const compressImage = {
  methods: {
    onFileSelected(event, type = 'single') {
      if (type === 'single') {
        this.singleImg = [];
      } else if (type === 'multiple') {
        this.multipleImg = [];
      }
      this.previewImage(event);
      for (let img of event.target.files) {
        this.selectedFile = img;
        console.log('selected file for compress is: ', this.selectedFile);
        this.handleImageUpload(img, type);
      }
    },
    previewImage(event) {
      var input = event.target;
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = (e) => {
          this.imageData = e.target.result;
        };
        reader.readAsDataURL(input.files[0]);
      }
    },
    handleImageUpload(img, type) {
      var imageFile = img;
      console.log('originalFile instanceof Blob', imageFile instanceof File); // true
      console.log(`originalFile size ${imageFile.size / 1024 / 1024} MB`);

      var options = {
        maxSizeMB: 1,
        maxWidthOrHeight: 1920,
        useWebWorker: false
      };

      imageCompression(imageFile, options)
        .then((compressedFile) => {
          console.log('compressedFile instanceof Blob', compressedFile instanceof File); // true
          console.log(`compressedFile size ${compressedFile.size / 1024 / 1024} MB`); // smaller than maxSizeMB
          console.log('after compress: ', compressedFile);
          if (type === 'single') {
            this.singleImg.push(compressedFile);
            console.log(this.singleImg);
          } else if (type === 'multiple') {
            this.multipleImg.push(compressedFile);
            console.log(this.multipleImg);
          }
        })
        .catch(function(error) {
          console.log(error.message);
        });
    }
  }
};

export default compressImage;

// import imageCompression from 'browser-image-compression';
// import { mapState } from 'vuex';

// const compressImage = {
//   methods: {
//     onFileSelected(event) {
//       this.previewImage(event);
//       this.selectedFile = event.target.files[0];
//       console.log('selected file for compress is: ', this.selectedFile);
//       this.image = this.handleImageUpload(event);
//       console.log('image returned by compressor: ', this.image);
//     },
//     previewImage(event) {
//       var input = event.target;
//       if (input.files && input.files[0]) {
//         var reader = new FileReader();
//         reader.onload = (e) => {
//           this.imageData = e.target.result;
//         };
//         reader.readAsDataURL(input.files[0]);
//       }
//     },
//     handleImageUpload(event) {
//       var imageFile = event.target.files[0];
//       console.log('originalFile instanceof Blob', imageFile instanceof File); // true
//       console.log(`originalFile size ${imageFile.size / 1024 / 1024} MB`);

//       var options = {
//         maxSizeMB: 1,
//         maxWidthOrHeight: 1920,
//         useWebWorker: false
//       };

//       imageCompression(imageFile, options)
//         .then((compressedFile) => {
//           console.log('compressedFile instanceof Blob', compressedFile instanceof File); // true
//           console.log(`compressedFile size ${compressedFile.size / 1024 / 1024} MB`); // smaller than maxSizeMB
//           console.log('after compress: ', compressedFile);
//           this.image = compressedFile;
//           console.log('blog image for testing is: ', this.image);
//         })
//         .catch(function(error) {
//           console.log(error.message);
//         });
//     }
//   }
// };

// export default compressImage;
