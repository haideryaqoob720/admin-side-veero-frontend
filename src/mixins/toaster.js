import { mapState } from 'vuex';

const toaster = {
  computed: {
    ...mapState('globalModule', {
      toast: (state) => state.toast
    })
  },
  watch: {
    toast: {
      handler() {
        console.log('changed detected in toaster');
        let globalToast = this.$store.state.globalModule.toast;
        globalToast.type === 'success'
          ? this.$toasted.success(globalToast.message)
          : this.$toasted.error(globalToast.message);
      },
      deep: true
    }
  }
};

export default toaster;
