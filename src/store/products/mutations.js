export default {
  getMvp(state, { globalModule }) {
    globalModule.loading = true;
    axios
      .get("/headerimage/getimage")
      .then(({ data }) => {
        state.mvp = data;
        globalModule.loading = false;
      })
      .catch((error) => {
        globalModule.loading = false;
      });
  },
  getProducts(state, { globalModule, page = 1 }) {
    globalModule.loading = true;
    axios
      .get(`/allProducts/${page}`)
      .then(({ data }) => {
        console.log(data);
        [state.products, state.resultsCount] = [data.rows, data.Records];
        globalModule.loading = false;
      })
      .catch((error) => {
        console.log(error);
        globalModule.loading = false;
      });
  },
  getMostPopular(state, { globalModule, page = 1 }) {
    globalModule.loading = true;
    axios
      .get(`/mostPopularProduct/dashboard`)
      .then(({ data }) => {
        console.log(data);
        state.mostPopular = data;
        globalModule.loading = false;
      })
      .catch((error) => {
        globalModule.loading = false;
      });
  },
  getPopularBrands(state, { globalModule, page = 1 }) {
    globalModule.loading = true;
    axios
      .get(`/popularProducts/dashboard`)
      .then(({ data }) => {
        state.popularBrands = data;
        globalModule.loading = false;
      })
      .catch((error) => {
        globalModule.loading = false;
      });
  },
  getBanners(state, { globalModule, page = 1 }) {
    globalModule.loading = true;
    axios
      .get(`banners/showAll/1`)
      .then(({ data }) => {
        console.log(data);
        [state.resultsCount, state.banners] = [data.Records, data.result[1]];
        globalModule.loading = false;
      })
      .catch((error) => {
        globalModule.loading = false;
      });
  },
};
