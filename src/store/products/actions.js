export default {
  getMvp({ commit, rootState }) {
    commit('getMvp', rootState);
  },
  getProducts({ commit, rootState }, page) {
    const globalModule = rootState.globalModule;
    commit('getProducts', { globalModule, page });
  },
  getMostPopular({ commit, rootState }, page) {
    const globalModule = rootState.globalModule;
    commit('getMostPopular', { globalModule, page });
  },
  getPopularBrands({ commit, rootState }, page) {
    const globalModule = rootState.globalModule;
    commit('getPopularBrands', { globalModule, page });
  },
  getBanners({ commit, rootState }, page) {
    const globalModule = rootState.globalModule;
    commit('getBanners', { globalModule, page });
  }
};
