export default {
  filteredProducts: (state) =>
    state.products.map(({ Id, Lowest_Ask, P_Name, Image_Name }) => {
      if (!Lowest_Ask) Lowest_Ask = '$ 0';
      return { id: Id, lowestAsk: Lowest_Ask, name: P_Name, image: Image_Name };
    }),
  filteredMostPopular: (state) =>
    state.mostPopular.map(({ Id, Image_Name, P_Name }) => {
      return { id: Id, image: Image_Name, name: P_Name };
    }),
  filteredPopularBrands: (state) =>
    state.popularBrands.map(({ Brand, Image_Id, Image_Name }) => {
      if (!Brand) Brand = 'Unknown Brand';
      return { id: Image_Id, image: Image_Name, name: Brand };
    }),
  filteredBanners: (state) =>
    state.banners.map(({ Banner_Id, Image_Id, Image_Name }) => {
      if (!Banner_Id) Banner_Id = 'Name not provided';
      return { name: Banner_Id, id: Image_Id, image: Image_Name };
    })
};
