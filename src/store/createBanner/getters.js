export default {
  filteredBrands: ({ brands }) =>
    brands.map(({ Id, Brand_Name, Image_Name }) => {
      return { id: Id, name: Brand_Name, img: Image_Name };
    }),
  filteredProducts: ({ products }) =>
    products.map(({ Id, P_Name, Description, Image_Name }) => {
      return { id: Id, name: P_Name, description: Description, img: Image_Name };
    })
};
