import router from '../../router/index';

const clearForm = (state) => {
  state.heading = '';
  state.description = '';
  state.selectedSingleImg = null;
};

export default {
  getBrands: (state, { globalModule }) => {
    globalModule.loading = true;
    axios
      .get('/brands/showAll/1')
      .then(({ data }) => {
        globalModule.loading = false;
        state.brands = data.result[1];
      })
      .catch((error) => {
        globalModule.loading = false;
      });
  },
  getProducts: (state, { globalModule }) => {
    globalModule.loading = true;
    axios
      .get('/adminproduct/all/Gucci')
      .then(({ data }) => {
        globalModule.loading = false;
        state.products = data.row;
      })
      .catch((error) => {
        globalModule.loading = false;
      });
  },
  createBanner: (state, { productsList, rootState }) => {
    console.log('create banner mutation');
    const toast = rootState.globalModule.toast;
    const fd = new FormData();
    const config = { headers: { 'Content-Type': 'multipart/form-data' } };
    fd.append('image', state.singleImg);
    // CREATE BANNER IMAGE
    const createBannerImage = axios.post('/banner1image', fd, config);
    const createBanner2nd = axios.post('/banners/create', {
      heading: state.heading,
      description: state.description
    });
    const createBanner3rd = axios.post('/bannersProducts/create', {
      brand_id: '1',
      products: productsList.join(',')
    });

    Promise.all([createBannerImage, createBanner2nd, createBanner3rd])
      .then(() => {
        [toast.watcher, toast.type, toast.message] = [Math.random(), 'success', 'Banner Created Successfully'];
        clearForm(state);
        router.go(-1);
      })
      .catch((error) => {
        [toast.watcher, toast.type, toast.message] = [Math.random(), 'error', 'Failed to create banner. Try Again!'];
        console.log(error.response);
      });
  }
};
