export default {
  getBrands: ({ commit, rootState }) => {
    commit('getBrands', rootState);
  },
  getProducts: ({ commit, rootState }) => {
    console.log('get product action');
    commit('getProducts', rootState);
  },
  createBanner: ({ commit, rootState }, productsList) => {
    console.log('create banner action');
    commit('createBanner', { rootState, productsList });
  }
};
