export default {
  getBuyers({ commit }, page) {
    commit('getBuyers', page);
  },
  getSellers({ commit }, page) {
    commit('getSellers', page);
  }
};
