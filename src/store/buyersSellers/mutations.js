export default {
  getBuyers(state, page = 1) {
    axios.get(`/adminbuyer/all/${page}`).then(({ data }) => {
      [state.resultsCount, state.buyers] = [data.Records, data.result[1]];
    });
  },
  getSellers(state, page = 1) {
    axios.get(`/adminsellers/allSellers/${page}`).then(({ data }) => {
      [state.resultsCount, state.sellers] = [data.Records, data.result[1]];
    });
  }
};
