export default {
  filteredBuyers: ({ buyers }) =>
    buyers.map(({ Id, First_Name, Last_Name, Email, User_Password, Status }) => {
      return {
        id: Id,
        firstName: First_Name,
        lastName: Last_Name,
        email: Email,
        password: User_Password,
        status: Status
      };
    }),
  filteredSellers: ({ buyers }) =>
    buyers.map(({ Id, First_Name, Last_Name, Email, User_Password, Status }) => {
      return {
        id: Id,
        firstName: First_Name,
        lastName: Last_Name,
        email: Email,
        password: User_Password,
        status: Status
      };
    })
};
