export default {
  getBids({ commit, rootState }, page) {
    const globalModule = rootState.globalModule;
    commit('getBids', { globalModule, page });
  },
  getAsks({ commit, rootState }, page) {
    const globalModule = rootState.globalModule;
    commit('getAsks', { globalModule, page });
  }
};
