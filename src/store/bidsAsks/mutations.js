export default {
  getBids(state, { globalModule }) {
    globalModule.loading = true;
    axios.get("/bids").then(({ data }) => {
      console.log(data);
      [state.bids, state.resultsCount] = [data.result, data.Records];
      globalModule.loading = false;
    });
  },
  getAsks(state, { globalModule }) {
    globalModule.loading = true;
    axios.get("/asks").then(({ data }) => {
      console.log(data);
      [state.asks, state.resultsCount] = [data.result, data.Records];
      globalModule.loading = false;
    });
  },
};
