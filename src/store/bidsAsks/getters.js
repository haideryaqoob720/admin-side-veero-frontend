export default {
  filteredBids: ({ bids }) =>
    bids.map(({ Bid, Id, Product_Id, Product_Name, User_Id, User_Name }) => {
      return {
        bid: Bid,
        bidId: Id,
        productId: Product_Id,
        productName: Product_Name,
        userId: User_Id,
        userName: User_Name
      };
    }),
  filteredAsks: ({ asks }) =>
    asks.map(({ Ask, Id, Product_Id, Product_Name, User_Id, User_Name }) => {
      return {
        ask: Ask,
        askId: Id,
        productId: Product_Id,
        productName: Product_Name,
        userId: User_Id,
        userName: User_Name
      };
    })
};
