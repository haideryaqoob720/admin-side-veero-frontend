export default {
  getBrands: ({ commit, rootState }) => {
    commit('getBrands', rootState);
  },
  getProducts: ({ commit, rootState }) => {
    console.log('get product action');
    commit('getProducts', rootState);
  },
  createMostPopular: ({ commit, rootState }, productsList) => {
    console.log('most popular action executing');
    commit('createMostPopular', { rootState, productsList });
  }
};
