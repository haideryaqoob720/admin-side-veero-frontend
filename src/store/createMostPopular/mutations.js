import router from '../../router/index';

export default {
  getBrands: (state, { globalModule }) => {
    globalModule.loading = true;
    axios
      .get('/brands/showAll/1')
      .then(({ data }) => {
        globalModule.loading = false;
        state.brands = data.result[1];
      })
      .catch((error) => {
        globalModule.loading = false;
      });
  },
  getProducts: (state, { globalModule }) => {
    globalModule.loading = true;
    axios
      .get('/adminproduct/all/Gucci')
      .then(({ data }) => {
        globalModule.loading = false;
        state.products = data.row;
      })
      .catch((error) => {
        globalModule.loading = false;
      });
  },
  createMostPopular: (state, { rootState, productsList }) => {
    const toast = rootState.globalModule.toast;
    axios
      .post('/mostPopularProduct/create', { products: productsList })
      .then((res) => {
        [toast.watcher, toast.type, toast.message] = [Math.random(), 'success', 'Create most popular successfully'];
        router.go(-1);
      })
      .catch((error) => {
        [toast.watcher, toast.type, toast.message] = [
          Math.random(),
          'error',
          'Failed to create most popular. Try Again.'
        ];
      });
  }
};
