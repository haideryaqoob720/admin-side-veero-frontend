import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import globalModule from './globalModule/index';
import login from './login/index';
import products from './products/index';
import updateMvp from './updateMvp/index';
import bidsAsks from './bidsAsks/index';
import buyersSellers from './buyersSellers/index';
import createProduct from './createProduct/index';
import createMostPopular from './createMostPopular/index';
import createBanner from './createBanner/index';
import orders from './orders/index';
import createPopularBrand from './createPopularBrand/index';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    login,
    products,
    updateMvp,
    bidsAsks,
    buyersSellers,
    createProduct,
    createPopularBrand,
    createMostPopular,
    createBanner,
    orders,
    globalModule
  }
});
