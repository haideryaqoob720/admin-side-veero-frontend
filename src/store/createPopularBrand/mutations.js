const clearForm = (state) => {
  state.name = '';
  state.description = '';
  state.selectedSingleImg = null;
};

export default {
  // HANDLILNG FORM DATA
  updateName: (state, payload) => (state.name = payload),
  updateDescription: (state, payload) => (state.description = payload),
  updateSingleImg: (state, payload) => (state.singleImg = payload),
  updateSelectedPopularBrand: (state, payload) => (state.selectedSingleImg = payload),

  // CREATING POPULAR BRAND
  createPopularBrand: (state, { globalModule }) => {
    globalModule.loading = true;
    const toast = globalModule.toast;

    // CREATING BRAND API
    const createPopularBrand = axios.post('/brands/create', {
      name: state.name,
      description: state.description
    });

    // CREATING BRAND WITH IMAGE API
    const fd = new FormData();
    fd.append('image', ...state.singleImg);
    const config = { headers: { 'Content-Type': 'multipart/form-data' } };
    const createPopularBrandImg = axios.post(`/BrandImages/Gucci`, fd, config);

    // CONCURRENT REQUESTS
    Promise.all([createPopularBrand, createPopularBrandImg])
      .then((res) => {
        [toast.watcher, toast.type, toast.message] = [Math.random(), 'success', 'Popular brand created successfully'];
        globalModule.loading = false;
        clearForm(state);
      })
      .catch((error) => {
        [toast.watcher, toast.type, toast.message] = [Math.random(), 'error', 'Failed to create popular brand'];
        globalModule.loading = false;
      });
  }
};
