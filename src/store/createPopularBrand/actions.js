export default {
  formValidityCheck: ({ state }) => state.name && state.description && state.singleImg.length,
  createBrand: ({ commit, dispatch, rootState }) => {
    const toast = rootState.globalModule.toast;
    dispatch('formValidityCheck').then((check) => {
      if (check) commit('createPopularBrand', rootState);
      else [toast.watcher, toast.type, toast.message] = [Math.random(), 'error', 'All the fields are required'];
    });
  }
};
