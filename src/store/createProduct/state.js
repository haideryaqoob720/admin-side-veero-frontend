export default {
  brand: '',
  name: '',
  price: '',
  retail: '',
  condition: '',
  description: '',
  singleImg: [],
  multipleImg: [],
  selectedSingleImg: null,
  selectedMultipleImg: null
};
