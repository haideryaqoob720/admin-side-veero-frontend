const clearForm = (state) => {
  state.brand = '';
  state.name = '';
  state.price = '';
  state.retail = '';
  state.condition = '';
  state.description = '';
  state.selectedSingleImg = null;
  state.selectedMultipleImg = null;
};

export default {
  // HANDLING FORMS
  updateBrand: (state, payload) => (state.brand = payload),
  updateName: (state, payload) => (state.name = payload),
  updatePrice: (state, payload) => (state.price = payload),
  updateRetail: (state, payload) => (state.retail = payload),
  updateCondition: (state, payload) => (state.condition = payload),
  updateDescription: (state, payload) => (state.description = payload),
  updateSingleImg: (state, payload) => (state.singleImg = payload),
  updateMultipleImg: (state, payload) => (state.multipleImg = payload),
  updateSelectedSingleImg: (state, payload) => (state.selectedSingleImg = payload),
  updateSelectedMultipleImg: (state, payload) => (state.selectedMultipleImg = payload),

  // CREATING PRODUCT
  createProduct(state, { globalModule }) {
    globalModule.loading = true;
    const toast = globalModule.toast;

    // PRODUCT API
    const updateProductData = {
      name: state.name,
      price: state.price + '',
      retail: state.retail + '',
      condition: state.condition,
      description: state.description,
      brand: state.brand
    };
    const updateProduct = axios.post('/adminproduct/create', updateProductData);

    // PRODUCT IMAGE API
    const config = { headers: { 'Content-Type': 'multipart/form-data' } };
    const fd = new FormData();
    fd.append('image', [...state.singleImg, ...state.multipleImg]);
    const updateProductImg = axios.post('/productImages', fd, config);

    Promise.all([updateProduct, updateProductImg])
      .then((res) => {
        [toast.watcher, toast.type, toast.message] = [Math.random(), 'success', 'Product Created Successfully'];
        globalModule.loading = false;
        clearForm(state);
      })
      .catch((error) => {
        [toast.watcher, toast.type, toast.message] = [Math.random(), 'error', 'Failed! Try Again'];
        globalModule.loading = false;
      });
  }
};
