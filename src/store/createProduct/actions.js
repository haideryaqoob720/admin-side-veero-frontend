export default {
  verifyInputs: ({ state }) => {
    return !!(
      state.brand &&
      state.name &&
      state.price &&
      state.retail &&
      state.condition &&
      state.description &&
      +state.singleImg.length &&
      +state.multipleImg.length
    );
  },
  createProduct({ commit, dispatch, rootState }) {
    const toast = rootState.globalModule.toast;
    dispatch('verifyInputs').then((check) => {
      if (check) commit('createProduct', rootState);
      else [toast.watcher, toast.type, toast.message] = [Math.random(), 'error', 'All the information is required'];
    });
  }
};
