export default {
  // check if the form is filled
  formCheck: ({ state }) => !!state.singleImg.length,

  // uploading image
  uploadImage({ commit, dispatch, rootState }) {
    const toast = rootState.globalModule.toast;

    dispatch('formCheck').then((check) => {
      if (check) commit('uploadImage', rootState);
      else [toast.watcher, toast.type, toast.message] = [Math.random(), 'error', 'Image is required'];
    });
  }
};
