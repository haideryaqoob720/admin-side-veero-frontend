const clearingForm = (state) => {
  state.selectedSingleImg = null;
};

export default {
  // HANDLING FORMDATA
  updateSelectedSingleImg: (state, payload) => (state.selectedSingleImg = payload),
  updateSelectedFile: (state, payload) => (state.selectedFile = payload),
  updateImage: (state, payload) => (state.singleImg = payload),

  // UPDATING FORMDATA
  uploadImage(state, { globalModule }) {
    const config = { headers: { 'Content-Type': 'multipart/form-data' } };
    const fd = new FormData();
    const toast = globalModule.toast;
    globalModule.loading = true;
    fd.append('image', ...state.singleImg);
    axios
      .post('/headerimage', fd, config)
      .then((response) => {
        // TOASTER
        [toast.watcher, toast.type, toast.message] = [Math.random, 'success', 'MVP Updated Successfully'];
        globalModule.loading = false;

        // CLEARING FORM DATA
        clearingForm(state);
      })
      .catch((error) => {
        [toast.watcher, toast.type, toast.message] = [Math.random, 'error', 'Uploading Failed! Try Again'];
        globalModule.loading = false;
      });
  }
};
