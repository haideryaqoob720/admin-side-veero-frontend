import io from "socket.io-client";
export default {
  getPendingOrders: (state, { globalModule }) => {
    const socket = io(process.env.VUE_APP_SERVICE_URL);
    globalModule.loading = true;
    socket.on("initial orders", (data) => {
      globalModule.loading = false;
      state.pending = data;
    });
  },
  getApprovedOrders: (state, { globalModule, page = 1 }) => {
    globalModule.loading = true;
    axios
      .get(`/adminorder/approved/${page}`)
      .then(({ data }) => {
        globalModule.loading = false;
        [state.resultsCount, state.approved] = [data.Records, data.rows];
      })
      .catch(() => {
        globalModule.loading = false;
      });
  },
  approveOrder: (state, { orderId, rootState }) => {
    const toast = rootState.globalModule.toast;
    console.log(state.pending);
    axios
      .post(`/adminorder/approveOrder/${orderId}`)
      .then(() => {
        state.pending = state.pending.filter((order) => order.Id != orderId);
        [toast.watcher, toast.type, toast.message] = [
          Math.random(),
          "success",
          "order approved",
        ];
        console.log(state.pending);
      })
      .catch((err) => {
        console.log(err);
        [toast.watcher, toast.type, toast.message] = [
          Math.random(),
          "error",
          "not approved, try again",
        ];
      });
  },
};
