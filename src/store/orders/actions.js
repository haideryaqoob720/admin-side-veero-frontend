export default {
  getPendingOrders: ({ commit, rootState }) => {
    commit('getPendingOrders', rootState);
  },
  getApprovedOrders: ({ commit, rootState }, page) => {
    const globalModule = rootState.globalModule;
    commit('getApprovedOrders', { globalModule, page });
  },
  approveOrder: ({ commit, rootState }, orderId) => {
    commit('approveOrder', { orderId, rootState });
  }
};
