export default {
  checkEntry: ({ state }) => {
    let [email, psswrd] = [state.email, state.psswrd];
    if (!email) {
      [state.toast.type, state.toast.message, state.toast.watcher] = ['error', 'Enter Email', Math.random()];
      return false;
    } else if (!psswrd) {
      [state.toast.type, state.toast.message, state.toast.watcher] = ['error', 'Enter Password', Math.random()];
      return false;
    }
    return true;
  },
  updateEmail({ commit }, value) {
    commit('updateEmail', value);
  },
  updatePassword({ commit }, value) {
    commit('updatePassword', value);
  },
  validatingLogin({ commit, dispatch, rootState }) {
    dispatch('checkEntry').then((check) => {
      if (check) {
        commit('loginValidity', rootState);
      }
    });
  }
};
