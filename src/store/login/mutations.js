import router from '../../router/index';

export default {
  updateEmail: (state, value) => {
    state.email = value;
  },
  updatePassword: (state, value) => {
    state.psswrd = value;
  },
  loginValidity(state, { globalModule }) {
    globalModule.loading = true;
    axios
      .post('/signin/admin', {
        email: state.email,
        psswrd: state.psswrd
      })
      .then(({ data }) => {
        globalModule.loading = false;
        router.push('/products');
        localStorage.setItem('token', data.token);
        localStorage.setItem('id', data.id);
        state.toast.watcher = Math.random();
        [state.toast.type, state.toast.message] = ['success', 'You have logged in'];
      })
      .catch((error) => {
        globalModule.loading = false;
        state.toast.type = Math.random();
        state.toast.watcher = Math.random();
        [state.toast.type, state.toast.message] = ['error', 'User does not exist'];
      });
  }
};
